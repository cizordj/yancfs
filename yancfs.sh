#!/bin/sh

help() {
    cat <<HELP
Usage:
./yancfs.sh <action>

Where <action> can be any of these:

init		-- Initialize all git submodules
setup		-- Install all modules dependencies
HELP
}

init_modules() {
    git submodule update --init --recursive
}

setup() {
    if [ ! -d ~/.config/phpactor ]
    then
        mkdir ~/.config/phpactor
    fi
    if [ -e ~/.config/phpactor/phpactor.json ]
    then
        rm ~/.config/phpactor/phpactor.json
    fi

    cp -r templates/ ~/.config/phpactor/
    cat <<'JSON' > ~/.config/phpactor/phpactor.json
{
    "indexer.exclude_patterns": [
        "/vendor/**/Tests/**/*",
        "/vendor/**/tests/**/*",
        "/var/cache/**/*",
        "/vendor/composer/**/*"
    ],
    "completion_worse.completor.keyword.enabled": false,
    "symfony.enabled": true,
    "language_server_code_transform.import_globals": true,
    "logging.enabled": true,
    "code_transform.import_globals": true,
    "code_transform.class_new.variants": {
        "strict": "strict_class",
        "test_case": "test_case",
        "final_readonly_strict_class": "final_readonly_strict_class"
    },
    "code_transform.refactor.generate_accessor.prefix": "get"
}
JSON
}

test ! $# -gt 0
THERE_IS_NO_ARGUMENT=$?

while true
do
    if [ $THERE_IS_NO_ARGUMENT -eq 0 ]; then
        init_modules
        setup
        exit 0
    elif [ "$1" = "-h" ] || [ "$1" = "--help" ]; then
        help
        exit 0
    elif [ "$1" = "init" ]; then
        init_modules
        exit 0
    elif [ "$1" = "setup" ]; then
        setup
        exit 0
    elif [ $# -eq 1 ]; then
        echo "Unknown action: $1"
        exit 1
    else
        break
    fi
    shift 1
done
