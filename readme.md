# yancfs

Pronounced
`jˈæŋkˌɛfˈɛs`,
yancfs is **Yet Another Neovim Configuration From Scratch**.

<div align="center">
<img src="logo.svg" />
</div>

This is my personal NeoVim configuration written entirely from scratch using pure Lua
and no plugin manager whatsoever.

#### Usage

- Clone this repo into `~/.config/nvim`
- Let it configure itself with `./yancfs.sh`

#### Goals and inspiration

The main idea behind this project is to create an IDE layer without
sacrificing Neovim vanilla features such as native packages and
keybindings.

The less configuration the better.
