local opts = {
  buf = 0
}
vim.api.nvim_set_option_value('shiftwidth', 1, opts)
vim.api.nvim_set_option_value('tabstop', 1, opts)
