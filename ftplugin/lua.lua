local opts = {
  buf = 0
}
vim.api.nvim_set_option_value('shiftwidth', 2, opts)
vim.api.nvim_set_option_value('tabstop', 2, opts)
vim.api.nvim_set_option_value('expandtab', true, opts)
