vim.bo.shiftwidth = 4
vim.bo.tabstop = 4
vim.api.nvim_set_var('php_folding', true)
vim.wo.foldlevel = 1
vim.bo.expandtab = true
