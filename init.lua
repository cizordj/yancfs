-- vim:foldmethod=marker

-- Global options {{{
vim.cmd.helptags("ALL")
-- mouse integration
vim.o.mouse = nil
-- show line numbers
vim.o.number = true
vim.o.timeoutlen = 300
-- Remove the annoying preview window
vim.o.completeopt = 'menu'

-- colorscheme stuff
vim.g.guifont = "JetBrains Mono"
vim.api.nvim_set_option('termguicolors', true)
-- vim.o.background = 'dark'
vim.cmd.colorscheme("onedark_dark")

-- notifications
local notify = require('notify')
notify.setup({
  background_colour = "#000000",
  top_down = false
})
vim.notify = notify

-- DBUI config
vim.g.db_ui_user_nerd_fonts = true
vim.g.db_ui_auto_execute_table_helpers = true
vim.g.db_ui_win_position = 'right'
vim.g.db_ui_show_database_icon = 1

-- Neovide
if (vim.g.neovide)
then
  require('yancfs.functions').setupNeovide()
end

-- Dictionary
require('yancfs.functions').loadDebianDictionaries()

vim.g.codeium_filetypes_disabled_by_default = true
vim.g.codeium_filetypes = {
  typescript =  true,
  typescriptreact = true,
  javascript = true,
  javascriptreact = true
}

-- }}}

-- Environment {{{
require('yancfs.binPath.handle')()
require('yancfs.keyboard-speed.handle')()
-- }}}

-- Plugins setups {{{
require("nvim-tree").setup({
  sort_by = "case_sensitive",
  view = {
    adaptive_size = true,
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = false,
  },
  git = {
    ignore = false,
  },
})
require('gitsigns').setup({
  on_attach = function()
    local gitsigns = require('gitsigns.actions')
    vim.keymap.set('n', '<leader>ga', [[<cmd>Git add %<CR>]],
      { noremap = true })
    vim.keymap.set('n', '<leader>gs', gitsigns.stage_hunk, { noremap = true })
    vim.keymap.set('n', '<leader>gd', gitsigns.diffthis, { noremap = true })
    vim.keymap.set('n', '<leader>gl', gitsigns.blame_line, { noremap = true })
  end
})
require('lualine').setup({
  options = {
    theme = 'auto',
    disabled_filetypes = {
      statusline = { "NvimTree" },
    }
  },
  tabline = {
    lualine_a = { 'buffers' },
    lualine_z = { 'tabs', 'datetime' },
  }
})
require 'colorizer'.setup()
-- }}}

-- Keybindings {{{
vim.g.mapleader = ','

-- window resize
vim.api.nvim_set_keymap('n', '<C-Up>', '<cmd>resize -1<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-Down>', '<cmd>resize +1<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-Left>', '<cmd>vertical resize -1<CR>', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-Right>', '<cmd>vertical resize +1<CR>', { noremap = true })
-- buffer navigation
vim.api.nvim_set_keymap('n', '<C-h>', '<cmd>bp<cr>', { noremap = true })
vim.api.nvim_set_keymap('n', '<C-l>', '<cmd>bn<cr>', { noremap = true })

-- code manipulation
vim.api.nvim_set_keymap('x', 'K', ":move '<-2<CR>gv-gv", { noremap = true })
vim.api.nvim_set_keymap('x', 'J', ":move '>+1<CR>gv-gv", { noremap = true })
vim.api.nvim_set_keymap('v', '<', '<gv', { noremap = true })
vim.api.nvim_set_keymap('v', '>', '>gv', { noremap = true })

local nvim_tree = require('nvim-tree.api').tree
vim.keymap.set('n', '<F2>', function() nvim_tree.toggle(true) end,
  { noremap = true, desc = "See the current file in the file manager" })
vim.keymap.set('n', '<F3>', nvim_tree.toggle, { noremap = true, desc = "Open up the file manager" })

vim.api.nvim_set_keymap('n', '<F4>', '<cmd>TagbarToggle<CR>', { noremap = true, desc = "Open up the tagbar" })
vim.keymap.set('n', '<F5>', require('yancfs.functions').loadUpDadbod,
  { noremap = false, desc = "Load up the database viewer" })

-- telescope
local telescope = require('telescope.builtin')
vim.keymap.set('n', '<leader>ff', telescope.find_files, { noremap = true })
vim.keymap.set('n', '<leader>fg', telescope.live_grep, { noremap = true })
vim.keymap.set('n', '<leader>fb', telescope.buffers, { noremap = true })
vim.keymap.set('n', '<leader>gs', telescope.git_status, { noremap = true })
-- require('telescope').setup({
--   defaults = {
--     preview = false
--   }
-- })

-- copy to system clipboard
vim.keymap.set(
  'v',
  '<C-y>',
  '"+y',
  { noremap = true, desc = "Copy to system clipboard" }
)

-- }}}

-- Command {{{
local create_command = vim.api.nvim_create_user_command

create_command(
  "DeleteFile",
  ":call delete(expand('%')) | bdelete!",
  {}
)
create_command(
  "FixWhitespace",
  [[:%s/\s\+$//e]],
  {}
)
create_command(
  "GAmmend",
  ":Git commit --amend --verbose",
  {}
)
create_command(
  "GCommit",
  ":Git commit --verbose",
  {}
)
create_command(
  "SetupGitubIdentity",
  require('yancfs.git.identities').setupGithubIdentity,
  {}
)
create_command(
  "SetupSenaiIdentity",
  require('yancfs.git.identities').setupSenaiIdentity,
  {}
)
create_command(
  "SetupCodebergIdentity",
  require('yancfs.git.identities').setupCodebergIdentity,
  {}
)
create_command(
  "SetupCniIdentity",
  require('yancfs.git.identities').setupCniIdentity,
  {}
)
create_command(
  "JsonPrettify",
  [[:%!jq '.']],
  {}
)
create_command(
  "JsonUglify",
  [[:%!jq --compact-output '.']],
  {}
)
create_command(
  "PgFormat",
  [[:%!pg_format -B -g -L -b -e -]],
  {}
)
create_command(
  "RebaseCurrentBranch",
  require('yancfs.functions').rebaseCurrentBranch,
  {}
)
create_command(
  "ReviseCode",
  ":Git diff develop HEAD",
  {}
)
create_command(
  "SortByLength",
  [[:'<,'>!awk '{ print length(), $0 | "sort -n | cut -d\\  -f2-" }']],
  {
    range = true
  }
)
-- }}}

-- Shortcuts {{{
-- No one is really happy until you have these shortcuts

vim.cmd(":abbreviate W! w!")
vim.cmd(":abbreviate W w")
vim.cmd(":abbreviate WQ wq")
vim.cmd(":abbreviate WQ! wq!")
vim.cmd(":abbreviate Wq wq")
-- }}}

-- Lsp configs {{{

require('yancfs.lsp.handle')()
require('vim.lsp.log').set_format_func(vim.inspect)
-- vim.lsp.set_log_level("DEBUG")

-- }}}

-- Treesitter {{{

require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all" (the listed parsers MUST always be installed)
  ensure_installed = { "c", "lua", "vim", "vimdoc", "query", "markdown", "markdown_inline", "typescript" },

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- Automatically install missing parsers when entering buffer
  -- Recommendation: set to false if you don't have `tree-sitter` CLI installed locally
  auto_install = true,

  -- List of parsers to ignore installing (or "all")
  ignore_install = {},

  ---- If you need to change the installation directory of the parsers (see -> Advanced Setup)
  -- parser_install_dir = "/some/path/to/store/parsers", -- Remember to run vim.opt.runtimepath:append("/some/path/to/store/parsers")!

  highlight = {
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    disable = { "c", "rust" },
    -- Or use a function for more flexibility, e.g. to disable slow treesitter highlight for large files
    -- disable = function(lang, buf)
    --     local max_filesize = 100 * 1024 -- 100 KB
    --     local ok, stats = pcall(vim.loop.fs_stat, vim.api.nvim_buf_get_name(buf))
    --     if ok and stats and stats.size > max_filesize then
    --         return true
    --     end
    -- end,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}

-- }}}
