local M = {}

---Rebase the current git branch interactively using the
---“develop” branch as an entry point. This is especially
---useful if you follow the git-flow model and want to squash
---your changes before the next release.
---This function won't do anything if it detects that you're
---not in a git-flow feature branch.
function M.rebaseCurrentBranch()
  local currentBranch = vim.api.nvim_exec("Git rev-parse --abbrev-ref HEAD", true)
  local weAreOnGitFlow = string.match(currentBranch, [[^feature/.*]])
  if not weAreOnGitFlow then
    return
  end
  local numberOfCommits = vim.api.nvim_exec("Git rev-list --count HEAD ^develop", true)
  local command = "Git rebase -i HEAD~" .. numberOfCommits
  return vim.api.nvim_command(command)
end

---@return nil
function M.loadUpDadbod()
  vim.cmd("packadd vim-dadbod.git")
  vim.cmd("packadd vim-dadbod-ui")
  vim.cmd("packadd vim-dadbod-completion")
  vim.keymap.set(
    'n',
    '<F5>',
    ':call db_ui#toggle()<cr>',
    {
      noremap = true,
      desc = 'Open up the database viewer'
    }
  )
  vim.cmd("helptags ALL")
  vim.cmd("DBUIToggle")
end

---@return nil
function M.setupNeovide()
  local cursor_particles = {
    "railgun",
    "torpedo",
    "pixiedust",
    "sonicboom",
    "ripple",
    "wireframe"
  }
  vim.o.guifont = "JetBrainsMono Nerd Font:h14"
  local i = math.random(0, #(cursor_particles) - 1)
  vim.g.neovide_cursor_vfx_mode = cursor_particles[i];
  vim.keymap.set(
    'n',
    '<C-S-PageUp>',
    function()
      vim.g.neovide_scale_factor =
          vim.g.neovide_scale_factor + 0.1
    end
  )
  vim.keymap.set(
    'n',
    '<C-S-PageDown>',
    function()
      vim.g.neovide_scale_factor =
          vim.g.neovide_scale_factor - 0.1
    end
  )
  vim.keymap.set(
    'n',
    '<C-S-Home>',
    function()
      vim.g.neovide_scale_factor = 1
    end
  )
  vim.keymap.set(
    { 'c', 'i' },
    '<C-S-v>',
    '<C-R>+'
  )
end

function M.loadDebianDictionaries()
  --- Dictionaries in a typical Debian system
  local folder = '/usr/share/dict/'
  local ok, dictionaries = pcall(vim.fn.readdir, folder)
  if not ok then
    return
  end
  for _, filename in ipairs(dictionaries) do
    vim.opt.dictionary:append(folder .. filename)
  end
end

return M
