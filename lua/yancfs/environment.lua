local M = {}

---Adds the given directory to the $PATH variable
---@param directory string Absolute paths are preferable
---@return nil
function M.addPath(directory)
  local setOfPaths = require('yancfs.ds.set')(
    M.splitPath()
  )
  setOfPaths[directory] = true
  local path = ''
  for dir, _ in pairs(setOfPaths) do
    path = path == '' and dir or string.format('%s:%s', path, dir)
  end
  vim.env.PATH = path
end

function M.splitPath()
  local t = {}
  for str in string.gmatch(vim.env.PATH, "([^:]+)") do
    table.insert(t, str)
  end
  return t
end

return M
