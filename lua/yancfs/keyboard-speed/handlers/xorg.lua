local M = {}

---@return boolean
function M.matches()
  return
      vim.env.XDG_SESSION_TYPE == "x11"
      and 1 == vim.fn.executable('xset')
end

---@return nil
function M.apply()
  os.execute([[xset r rate 200 40]])
end

return M
