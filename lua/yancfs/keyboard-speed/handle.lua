local handlers = {
  require('yancfs.keyboard-speed.handlers.xorg'),
}

local function handle()
  for _, handler in ipairs(handlers) do
    if handler.matches() then
      handler.apply()
    end
  end
end

return handle
