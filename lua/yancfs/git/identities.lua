local G = {}

---Makes the current git project use my identity
---from cni, which includes username, email,
---gpg key and so on.
function G.setupCniIdentity()
  vim.cmd(
    vim.fn.printf(
      'Git config user.name "%s"',
      vim.base64.decode(
        'Q8OpemFyIEF1Z3VzdG8gZGUgQ2FtcG9z'
      )
    )
  )
  vim.cmd(
    vim.fn.printf(
      'Git config user.email "%s"',
      vim.base64.decode(
        'dC1jZXphci5jYW1wb3NAY25pLmNvbS5icg=='
      )
    )
  )
  vim.cmd("Git config user.signingKey 16DC13CE15C3BA0053383E689466E26E3D20204C")
  vim.cmd("Git config commit.verbose true")
  vim.cmd("Git config commit.gpgSign true")
end

---Makes the current git project use my identity
---from codeberg, which includes username, email,
---gpg key and so on.
function G.setupCodebergIdentity()
  vim.cmd('Git config user.name cizordj')
  vim.cmd(
    vim.fn.printf(
      'Git config user.email "%s"',
      vim.base64.decode(
        'emtxdiszNjY2OUBjZXphcmNhbXBvcy5jb20uYnI='
      )
    )
  )
  vim.cmd("Git config user.signingKey 16DC13CE15C3BA0053383E689466E26E3D20204C")
  vim.cmd("Git config commit.verbose true")
  vim.cmd("Git config commit.gpgSign true")
end

---Makes the current git project use my identity
---from github, which includes username, email,
---gpg key and so on.
function G.setupGithubIdentity()
  vim.cmd("Git config user.name cizordj")
  vim.cmd(
    vim.fn.printf(
      'Git config user.email "%s"',
      vim.base64.decode(
        'MzI4NjkyMjIrY2l6b3JkakB1c2Vycy5ub3JlcGx5LmdpdGh1Yi5jb20='
      )
    )
  )
  vim.cmd("Git config user.signingKey 16DC13CE15C3BA0053383E689466E26E3D20204C")
  vim.cmd("Git config commit.verbose true")
  vim.cmd("Git config commit.gpgSign true")
end

---Makes the current git project use my identity
---from senai, which includes username, email,
---gpg key and so on.
function G.setupSenaiIdentity()
  vim.cmd(
    vim.fn.printf(
      'Git config user.name "%s"',
      vim.base64.decode(
        'Q8OpemFyIEF1Z3VzdG8gZGUgQ2FtcG9z'
      )
    )
  )
  vim.cmd(
    vim.fn.printf(
      'Git config user.email "%s"',
      vim.base64.decode(
        'Y2V6YXIuY2FtcG9zQHNjLnNlbmFpLmJy'
      )
    )
  )
  vim.cmd("Git config user.signingKey 43B75D61CB604745CA4D265A23AB483D091A85C9")
  vim.cmd("Git config commit.verbose true")
  vim.cmd("Git config commit.gpgSign true")
end

return G
