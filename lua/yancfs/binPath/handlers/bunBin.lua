local M = {}

local path = string.format('%s/%s', vim.env.HOME, '.bun/bin')

---@return boolean
function M.directoryExists()
  return 1 == vim.fn.isdirectory(path)
end

---@return nil
function M.apply()
  require('yancfs.environment').addPath(
    path
  )
end

return M
