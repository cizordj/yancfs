local handlers = {
  require('yancfs.binPath.handlers.bunBin'),
  require('yancfs.binPath.handlers.denoPath'),
  require('yancfs.binPath.handlers.homeBin'),
  require('yancfs.binPath.handlers.nodeBinFromProject'),
  require('yancfs.binPath.handlers.vendorBinFromProject'),
  require('yancfs.binPath.handlers.yarnGlobalBinPath')
}

local function handle()
  for _, handler in ipairs(handlers) do
    if handler.directoryExists() then
      handler.apply()
    end
  end
end

return handle
