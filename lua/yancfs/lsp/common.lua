local M = {}

---@param bufnr number
---@param clientName string
function M.setKeymaps(bufnr, clientName)
  vim.notify(
    string.format(
      'Setting keymaps in buffer %d for client %s',
      bufnr,
      clientName
    ),
    vim.log.levels.INFO
  )

  -- Mappings.
  -- See `:help vim.lsp.*` for documentation on any of the below functions
  local bufopts = { noremap = true, silent = true, buffer = bufnr }
  if vim.version()['minor'] == 9
  then
    vim.keymap.set('n', 'K', vim.lsp.buf.hover, bufopts)
    vim.keymap.set(
      'n',
      '<leader>q',
      function()
        vim.diagnostic.open_float({
          bufnr = bufnr,
          scope = "line"
        })
      end,
      bufopts
    )
  end
  vim.keymap.set('n', '<space>rn', vim.lsp.buf.rename, bufopts)
  vim.keymap.set('n', '<leader>ca', vim.lsp.buf.code_action, bufopts)
  vim.keymap.set('n', 'gr', vim.lsp.buf.references, bufopts)
  vim.keymap.set('n', '<leader>q', function()
    if vim.version().minor == 9 then
      vim.diagnostic.open_float({
        bufnr = bufnr,
        scope = "line"
      })
    else
      vim.print('Use <C-W>d instead')
    end
  end, bufopts)
  vim.keymap.set(
    'n',
    '<leader>Q',
    function()
      vim.diagnostic.open_float({
        bufnr = bufnr,
        scope = "buffer"
      })
    end,
    bufopts
  )
  vim.keymap.set(
    'n',
    '<leader>fm',
    vim.lsp.buf.format,
    bufopts
  )
end

return M
