local handlers = {
  require('yancfs.lsp.handlers.biome'),
  require('yancfs.lsp.handlers.clangd'),
  require('yancfs.lsp.handlers.cssls'),
  require('yancfs.lsp.handlers.denols'),
  require('yancfs.lsp.handlers.docker-compose'),
  require('yancfs.lsp.handlers.eslint'),
  require('yancfs.lsp.handlers.lemminx'),
  require('yancfs.lsp.handlers.jsonls'),
  require('yancfs.lsp.handlers.jdtls'),
  require('yancfs.lsp.handlers.lua-language-server'),
  require('yancfs.lsp.handlers.ltex'),
  require('yancfs.lsp.handlers.html'),
  require('yancfs.lsp.handlers.phpactor'),
  require('yancfs.lsp.handlers.sonarlint'),
  require('yancfs.lsp.handlers.tailwindcss'),
  require('yancfs.lsp.handlers.ts_ls'),
  require('yancfs.lsp.handlers.vala')
}

local function handle()
  for _, handler in ipairs(handlers) do
    if handler.matches() then
      handler.apply()
    end
  end
end

return handle
