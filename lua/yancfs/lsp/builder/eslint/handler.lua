local M = {}

local strategies = {
  require('yancfs.lsp.builder.eslint.strategies.bun'),
  require('yancfs.lsp.builder.eslint.strategies.node'),
  require('yancfs.lsp.builder.eslint.strategies.yarnpkg')
}

---@return table
function M.buildCommand ()
  for _, strategy in ipairs(strategies) do
    if strategy.matches() then
      return strategy.generateCommand()
    end
  end
  error('Command for eslint could not be built', 2)
end

return M
