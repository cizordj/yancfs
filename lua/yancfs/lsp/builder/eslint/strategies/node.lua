local M = {}

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('vscode-eslint-language-server')
end

---@return table
function M.generateCommand()
  return {
    'vscode-eslint-language-server',
    '--stdio'
  }
end

return M
