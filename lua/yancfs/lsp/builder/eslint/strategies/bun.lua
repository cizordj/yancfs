local M = {}

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('bunx')
end

---@return table
function M.generateCommand()
  return {
    'bunx',
    '--bun',
    'vscode-langservers-extracted@latest',
    'vscode-eslint-language-server',
    '--stdio'
  }
end

return M
