local M = {}

local strategies = {
  require('yancfs.lsp.builder.sonarlint.strategies.java'),
  require('yancfs.lsp.builder.sonarlint.strategies.sonarlint-lsp'),
}

---@return table
function M.buildCommand ()
  for _, strategy in ipairs(strategies) do
    if strategy.matches() then
      return strategy.generateCommand()
    end
  end
  error('Command for sonarlint could not be built', 2)
end

return M
