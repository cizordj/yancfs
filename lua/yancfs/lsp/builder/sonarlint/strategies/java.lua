local M = {}

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('java')
end

---@return table
function M.generateCommand()
  return {
    'java',
    '-jar',
    '/usr/local/share/sonarlint/server/sonarlint-ls.jar',
    '-stdio',
    '-analyzers',
    '/usr/local/share/sonarlint/analyzers/sonarcfamily.jar',
    '/usr/local/share/sonarlint/analyzers/sonargo.jar',
    '/usr/local/share/sonarlint/analyzers/sonarhtml.jar',
    '/usr/local/share/sonarlint/analyzers/sonariac.jar',
    '/usr/local/share/sonarlint/analyzers/sonarjava.jar',
    '/usr/local/share/sonarlint/analyzers/sonarjavasymbolicexecution.jar',
    '/usr/local/share/sonarlint/analyzers/sonarjs.jar',
    '/usr/local/share/sonarlint/analyzers/sonarlintomnisharp.jar',
    '/usr/local/share/sonarlint/analyzers/sonarphp.jar',
    '/usr/local/share/sonarlint/analyzers/sonarpython.jar',
    '/usr/local/share/sonarlint/analyzers/sonartext.jar',
    '/usr/local/share/sonarlint/analyzers/sonarxml.jar'
  }
end

return M
