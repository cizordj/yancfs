local M = {}

---@link https://codeberg.org/cizordj/sonarlint-makefile
---@return boolean
function M.matches()
  return 1 == vim.fn.executable('sonarlint-lsp')
end

---@return table
function M.generateCommand()
  return { 'sonarlint-lsp' }
end

return M
