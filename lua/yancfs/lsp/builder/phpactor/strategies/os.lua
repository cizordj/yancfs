local M = {}

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('phpactor')
end

---@return table
function M.generateCommand()
  return {
    "phpactor",
    "language-server"
  }
end

return M
