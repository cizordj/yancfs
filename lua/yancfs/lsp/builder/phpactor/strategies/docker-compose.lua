local M = {}

---@return boolean
function M.matches()
  local composeFiles = {
    "compose.override.yml",
    "compose.override.yaml",
  }

  for _, file in ipairs(composeFiles) do
    if 1 == vim.fn.filereadable(file) then
      return true
    end
  end
  return false
end

---@return table
function M.generateCommand()
  return {
      "docker",
      "compose",
      "run",
      "--rm",
      "--volume", string.format("%s/.cache/phpactor/:/tmp/cache/phpactor/", vim.env.HOME),
      "--volume", string.format("%s:%s", vim.fn.getcwd(), vim.fn.getcwd()),
      "--volume", string.format("%s/.config/phpactor/:/tmp/phpactor-config/phpactor/", vim.env.HOME),
      --TODO: Find a better way to put the binary inside
      "--volume",
      string.format(
        -- This is the absolute path to the phpactor binary
        "%s:/tmp/phpactor/bin/phpactor",
        string.format(
          "%s/.local/bin/phpactor",
          vim.env.HOME
        )
      ),
      "--name", string.format("%s_phpactor", vim.fn.fnamemodify(vim.fn.getcwd(), ":t")),
      "--workdir", vim.fn.getcwd(),
      "-e", "XDG_CACHE_HOME=/tmp/cache/",
      "-e", "XDG_CONFIG_HOME=/tmp/phpactor-config/",
      "-u", vim.fn.systemlist("id -u")[1],
      -- This service can be defined in compose.override.yml
      "phpactor",
      "php", "/tmp/phpactor/bin/phpactor",
      "language-server",
      "--no-ansi"
    }
end

return M
