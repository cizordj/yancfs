local M = {}

local strategies = {
  require('yancfs.lsp.builder.phpactor.strategies.docker-compose'),
  require('yancfs.lsp.builder.phpactor.strategies.os'),
}

---@return table
function M.buildCommand()
  for _, strategy in ipairs(strategies) do
    if strategy.matches() then
      return strategy.generateCommand()
    end
  end
  error('Command for phpactor could not be built', 2)
end

return M
