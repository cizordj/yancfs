local M = {}

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('deno')
end

---@return table
function M.generateCommand()
  return {
    'deno',
    'run',
    '--allow-env',
    '--allow-read',
    '--allow-write',
    '--allow-run',
    '--no-prompt',
    'npm:typescript-language-server',
    '--stdio'
  }
end

return M
