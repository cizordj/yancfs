local M = {}

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('typescript-language-server')
end

---@return table
function M.generateCommand()
  return {
    'typescript-language-server',
    '--stdio'
  }
end

return M
