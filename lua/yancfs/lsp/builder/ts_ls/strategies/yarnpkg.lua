local M = {}

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('yarnpkg')
end

---@return table
function M.generateCommand()
  return {
    'yarnpkg',
    'dlx',
    'typescript-language-server',
    '--stdio'
  }
end

return M
