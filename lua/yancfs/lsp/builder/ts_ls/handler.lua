local M = {}

local strategies = {
  require('yancfs.lsp.builder.ts_ls.strategies.deno'),
  require('yancfs.lsp.builder.ts_ls.strategies.yarnpkg'),
  require('yancfs.lsp.builder.ts_ls.strategies.bun'),
  require('yancfs.lsp.builder.ts_ls.strategies.node'),
}

---@return table
function M.buildCommand ()
  for _, strategy in ipairs(strategies) do
    if strategy.matches() then
      return strategy.generateCommand()
    end
  end
  error('Command for typescript-language-server could not be built', 2)
end

return M
