local M = {}

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('vscode-json-language-server')
end

---@return table
function M.generateCommand()
  return {
    'vscode-json-language-server',
    '--stdio'
  }
end

return M
