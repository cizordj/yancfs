local M = {}

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('lemminx')
end

---@return nil
function M.apply()
  return require('lspconfig')['lemminx'].setup {
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end
  }
end

return M
