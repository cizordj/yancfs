local M = {}

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('jdtls')
end

function M.apply()
  require('lspconfig')['jdtls'].setup {
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end,
    detached = false
  }
end

return M
