local M = {}

local commandGenerator = require('yancfs.lsp.builder.ts_ls.handler')

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  local ok, output = pcall(commandGenerator.buildCommand)
  if not ok then
    return false
  end
  return 1 == vim.fn.executable(output[1])
end

---@return nil
function M.apply()
  local ok, output = pcall(commandGenerator.buildCommand)
  if not ok then
    vim.notify(output, vim.log.levels.WARN)
    return
  end

  require('lspconfig')['ts_ls'].setup({
    cmd = output,
    autostart = false,
    handlers = {
      ["$/typescriptVersion"] = function(_, params)
        vim.notify(
          string.format(
            "Running typescript version %s from %s",
            params.version,
            params.source
          ),
          vim.log.levels.INFO
        )
      end
    },
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end,
    flags = {
      debounce_text_changes = 1000
    },
    detached = false,
    -- https://github.com/typescript-language-server/typescript-language-server/blob/master/docs/configuration.md#initializationoptions
    init_options = {
      hostInfo = "neovim",
      maxTsServerMemory = 6096,
      locale = "pt-br",
      preferences = {
        includeCompletionsForModuleExports = false,
        importModuleSpecifierEnding = 'minimal'
      }
    }
  })
end

return M
