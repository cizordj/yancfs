local M = {}

local commandGenerator = require('yancfs.lsp.builder.sonarlint.handler')

local common = require('yancfs.lsp.common')

local function get_snake_case_current_folder()
  local cwd = vim.fn.getcwd()
  local folder_name = cwd:match("^.+/(.+)$") or cwd
  local snake_case = folder_name:gsub("%s+", "_"):gsub("[^%w_]", ""):lower()
  return snake_case
end

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('sonarlint-lsp')
end

---@return nil
function M.apply()
  local lspconfig = require('lspconfig')
  local configs = require('lspconfig.configs')

  local ok, output = pcall(commandGenerator.buildCommand)

  if not ok then
    vim.notify(output, vim.log.levels.WARN)
    return
  end

  --- @class lspconfig.Config
  configs.sonarlint = {
    default_config = {
      cmd = output,
      autostart = false,
      handlers = {
        ["window/logMessage"] = function(_, params)
          vim.print(params.message)
        end,
        ["sonarlint/readyForTests"] = function()
          vim.notify("Sonarqube has started", vim.log.levels.INFO)
        end,
        ["sonarlint/reportConnectionCheckResult"] = function(_, params)
          local connectionId = params.connectionId
          local reason = params.reason
          local success = params.success
          if success then
            vim.notify(
              string.format(
                "Connected successfully to %s!",
                connectionId
              ),
              vim.log.levels.INFO
            )
          else
            vim.notify(
              string.format(
                "Connection to %s failed! Reason: %s",
                connectionId,
                reason
              ),
              vim.log.levels.ERROR
            )
          end
        end,
        ["sonarlint/isOpenInEditor"] = function(_, params)
          local file_path = vim.uri_to_fname(params[1])
          for _, buf in ipairs(vim.api.nvim_list_bufs()) do
            if vim.api.nvim_buf_is_loaded(buf) and vim.api.nvim_buf_get_name(buf) == file_path then
              return true
            end
          end
          return false
        end,
        ["sonarlint/filterOutExcludedFiles"] = function(_, params)
          return params
        end,
        ["sonarlint/getTokenForServer"] = function(_, params)
          local serverUrl = params[1]
          -- Return the token
          return serverUrl
        end,
        ["sonarlint/listFilesInFolder"] = function(_, params)
          local folderUri = vim.uri_to_fname(params.folderUri)
          local files = {
            foundFiles = {}
          }
          local uv = vim.loop

          -- Open the folder
          local handle = uv.fs_scandir(folderUri)
          if not handle then
            vim.notify("Cannot open folder: " .. folderUri, vim.log.levels.ERROR)
            return files
          end

          while true do
            local name, type = uv.fs_scandir_next(handle)
            if not name then
              break
            end
            -- FIXME: We should probably use plenary's path
            if type == "file" then
              table.insert(files.foundFiles,
                {
                  fileName = name,
                  filePath = folderUri
                }
              )
            end
          end

          return files
        end
      },
      detached = false,
      filetypes = {
        'java',
        'javascript',
        'javascriptreact',
        'typescript',
        'typescriptreact',
        'css',
        'html',
        'python',
        'cpp',
        'c',
        'php',
        'dockerfile'
      },
      root_dir = function(fname)
        if vim.version()['minor'] == 9
        then
          return lspconfig.util.find_git_ancestor(fname)
        else
          return vim.fs.root(0, '.git')
        end
      end,
      -- https://github.com/SonarSource/sonarlint-language-server/blob/3.11.1.75553/src/main/java/org/sonarsource/sonarlint/ls/settings/SettingsManager.java
      settings = {
        sonarlint = {
          -- connectedMode = {
          --   project = {
          --     projectKey = "",
          --     connectionId = ""
          --   },
          --   connections = {
          --     sonarqube = {
          --       {
          --         serverUrl = "",
          --         token = "",
          --         connectionId = ""
          --       },
          --     }
          --   }
          -- },
          showAnalyzerLogs = false,
          showVerboseLogs = false,
          disableTelemetry = false,
          ["files.exclude"] = {
            ["**/.git"] = true,
            ["**/node_modules"] = true,
          },
          rules = vim.empty_dict()
        }
      },
      -- https://github.com/SonarSource/sonarlint-language-server/blob/3.11.1.75553/src/main/java/org/sonarsource/sonarlint/ls/SonarLintLanguageServer.java#CompletableFuture
      init_options = {
        productKey = "yancfs.nvim",
        productName = "Yancfs.nvim",
        productVersion = "1.0",
        workspaceName = get_snake_case_current_folder(),
        firstSecretDetected = false,
        showVerboseLogs = false,
        platform = vim.loop.os_uname().sysname,
        architecture = vim.loop.os_uname().machine,
      },
      single_file_support = false
    },
    docs = {
      description = 'An advanced linter in your IDE for Clean Code',
    }
  }
  lspconfig.sonarlint.setup({
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end,
    flags = {
      debounce_text_changes = 3000
    },
  })
end

return M
