local M = {}

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('lua-language-server')
end

---@return nil
function M.apply()
  require('lspconfig')['lua_ls'].setup {
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end,
    settings = {
      Lua = {
        diagnostics = {
          globals = { "vim" }
        }
      }
    },
    workspace = {
      library = vim.api.nvim_get_runtime_file("", true),
    },
    telemetry = {
      enable = false,
    }
  }
end

return M
