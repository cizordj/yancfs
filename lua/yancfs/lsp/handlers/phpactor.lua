local M = {}

local common = require('yancfs.lsp.common')

local commandGenerator = require('yancfs.lsp.builder.phpactor.handler')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('phpactor')
end

---@return nil
function M.apply()
  local ok, output = pcall(commandGenerator.buildCommand)

  if not ok then
    vim.notify(output, vim.log.levels.WARN)
    return
  end

  require('lspconfig')['phpactor'].setup({
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end,
    cmd = output,
    filetypes = {
      'php'
    },
    detached = false
  })
end

return M
