local M = {}

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('docker-compose-langserver')
end

---@return nil
function M.apply()
  require('lspconfig')['docker_compose_language_service'].setup {
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end
  }
end

return M
