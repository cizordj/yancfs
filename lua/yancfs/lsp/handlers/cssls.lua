local M = {}

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('vscode-css-language-server')
end

---@return nil
function M.apply()
  require('lspconfig')['cssls'].setup {
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end
  }
end

return M
