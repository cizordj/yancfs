local M = {}

local commandGenerator = require('yancfs.lsp.builder.eslint.handler')

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  local executables = {
    'vscode-eslint-language-server',
    'deno',
    'bunx'
  }
  for _, program in ipairs(executables) do
    if 1 == vim.fn.executable(program) then
      return true
    end
  end
  return false
end

function M.apply()
  local ok, output = pcall(commandGenerator.buildCommand)
  if not ok then
    vim.notify(output, vim.log.levels.WARN)
    return
  end

  require('lspconfig')['eslint'].setup({
    cmd = output,
    autostart = false,
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end,
    detached = false
  })
end

return M
