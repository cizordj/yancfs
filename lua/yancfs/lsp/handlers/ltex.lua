local M = {}

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('ltex-ls')
end

--- Download it from https://github.com/valentjn/ltex-ls
---@return nil
function M.apply()
  require('lspconfig')['ltex'].setup({
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end,
    autostart = false,
    detached = false,
    settings = {
      ltex = {
        language = "pt-BR"
      }
    },
    filetypes = {
      'markdown'
    }
  })
end

return M
