local M = {}

local commandGenerator = require('yancfs.lsp.builder.jsonls.handler')

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  local executables = {
    'vscode-json-language-server',
    'deno',
    'bunx'
  }
  for _, program in ipairs(executables) do
    if 1 == vim.fn.executable(program) then
      return true
    end
  end
  return false
end

function M.apply()
  local ok, output = pcall(commandGenerator.buildCommand)
  if not ok then
    vim.notify(output, vim.log.levels.WARN)
    return
  end

  local capabilities = vim.lsp.protocol.make_client_capabilities()
  capabilities.textDocument.completion.completionItem.snippetSupport = true

  require('lspconfig')['jsonls'].setup {
    cmd = output,
    capabilities = capabilities,
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end
  }
end

return M
