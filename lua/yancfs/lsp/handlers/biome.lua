local M = {}

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('biome')
end

---@return nil
function M.apply()
  require('lspconfig')['biome'].setup {
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end
  }
end

return M
