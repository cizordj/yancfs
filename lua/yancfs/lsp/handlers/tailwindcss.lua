local M = {}

local common = require('yancfs.lsp.common')

---@return boolean
function M.matches()
  return 1 == vim.fn.executable('tailwindcss-language-server')
end

---@return nil
function M.apply()
  local cmd = {
    'tailwindcss-language-server',
    '--stdio'
  }
  if require('yancfs.binPath.handlers.bunBin').directoryExists() then
    cmd = {
      'bunx',
      '--bun',
      'tailwindcss-language-server',
      '--stdio'
    }
  end
  require('lspconfig')['tailwindcss'].setup {
    cmd,
    on_attach = function(client, bufnr)
      common.setKeymaps(bufnr, client.name)
    end,
  }
end

return M
